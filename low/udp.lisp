;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File Name:	  udp.lisp
;;; Description:  Datagram Service Provided by UDP
;;; Author:	  Simon Leinen (simon@liasun1)
;;; Date Created: 30-May-92
;;; RCS $Header: /home/leinen/CVS/lisp-snmp/low/udp.lisp,v 1.23 2004/01/01 09:56:53 leinen Exp $  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :snmp)

(defconstant default-udp-packet-buffer-size 8192)

(defstruct (udp-session
	    (:copier nil)
	    (:predicate nil)
	    (:print-function print-udp-session))
  (packet-buffer (make-array (list default-udp-packet-buffer-size)
			     :element-type '(unsigned-byte 8)))
  (remote-host nil :type (or null base-string ipaddr))
  (remote-port nil :type (or null (unsigned-byte 16)))
  #+Genera (remote-service-access-path)
  #-Genera (remote-address nil :type (or null network-address))
  (local-host nil :type (or null base-string ipaddr))
  (local-port nil :type (or null (unsigned-byte 16)))
  #-Genera (local-address nil :type (or null network-address))
  #+Genera (local-service-access-path)
  #-Genera (socket nil)
  #+:cmu (sap-buffer (get-packet-buffer-sap))
  #+:cmu (length-buffer (get-int-sap))
  #+(and excl (not (version>= 5 0))) (length-buffer (ff:malloc-cstruct 'c-int))
  #+:cmu (address-buffer (get-sockaddr-in-sap))
  #+(and excl (not (version>= 5 0))) (address-buffer (ff:malloc-cstruct 'ipc::sockaddr-in))
  (receive-buffer-size 0 :type (unsigned-byte 24)))

(defun print-udp-session (session stream level)
  (declare (ignore level))
  (print-unreadable-object (session stream :type t)
    (format stream "~:[*~;~-1*~A~]:~:[*~;~-1*~A~] ~:[*~;~-1*~A~]:~:[*~;~-1*~A~]"
	    (udp-session-local-host session)
	    (udp-session-local-port session)
	    (udp-session-remote-host session)
	    (udp-session-remote-port session))))

#+Genera
(progn

(defun initialize-udp-session (session)
  (labels ((get-service-acess-path (host-name port)
	     (and host-name port
		  (let ((host (net:parse-host host-name t)))
		    (if host
			(net:find-path-to-service-on-host port host)
		      (let ((host (neti:domain-parse-host host-name)))
			(unless host
			  (error 'unknown-host-error :host-name host-name))
			(net:find-path-to-protocol-on-host :snmp host)))))))
    (setf (udp-session-remote-service-access-path session)
      (get-service-access-path
       (udp-session-remote-host session)
       (udp-session-remote-port session)))
    (setf (udp-session-local-service-access-path session)
      (get-service-access-path
       (udp-session-local-host session)
       (udp-session-local-port session)))))

(defun close-udp-session (session)
  (declare (ignore session)))
)

(defun send-packet (session packet &key (start 0) (end (length packet)))
  (send-packet-to session packet (udp-session-remote-address session)
		  :start start :end end))

#-(or Genera (and allegro (version>= 5 0)))
(defun initialize-udp-session (session)
  (labels ((get-network-address (host port)
	     (host/port->network-address host port)))
    (setf (udp-session-local-address session)
      (get-network-address (udp-session-local-host session)
			   (udp-session-local-port session)))
    (setf (udp-session-remote-address session)
      (get-network-address (udp-session-remote-host session)
			   (udp-session-remote-port session))))
  (setf (udp-session-socket session) (open-udp-socket))
  (unless (= (udp-session-receive-buffer-size session) 0)
    (set-socket-receive-buffer-size
     (udp-session-socket session)
     (udp-session-receive-buffer-size session)))
  (when (or (udp-session-local-host session)
	    (udp-session-local-port session))
    (setf (udp-session-socket session)
      (bind-socket (udp-session-socket session)
		   (udp-session-local-address session))))
  session)

#+(and allegro (version>= 5 0))
(defun initialize-udp-session (session)
  (labels ((get-network-address (host port)
	     (host/port->network-address host port)))
    (setf (udp-session-local-address session)
      (get-network-address (udp-session-local-host session)
			   (udp-session-local-port session)))
    (setf (udp-session-remote-address session)
      (get-network-address (udp-session-remote-host session)
			   (udp-session-remote-port session))))
  (setf (udp-session-socket session)
    (if (udp-session-local-port session)
	(socket:make-socket :format :binary
			    :type :datagram
			    :connect :passive
			    :address-family :internet
			    :local-port (udp-session-local-port session))
      (socket:make-socket :format :binary
			  :type :datagram
			  :connect :passive
			  :address-family :internet)))
  (unless (= (udp-session-receive-buffer-size session) 0)
    (set-socket-receive-buffer-size
     (udp-session-socket session)
     (udp-session-receive-buffer-size session))))

#+(and excl (not (version>= 5 0)))

(progn
  
(defun open-udp-socket ()
  (ipc::socket af-inet sock-dgram 0))
  
(defun close-udp-session (session)
  (when (udp-session-local-address session)
    (free-network-address (udp-session-local-address session))
    (setf (udp-session-local-address session) nil))
  (when (udp-session-remote-address session)
    (free-network-address (udp-session-remote-address session))
    (setf (udp-session-remote-address session) nil))
  (when (udp-session-socket session)
    (ipc::unix-close (udp-session-socket session))
    (setf (udp-session-socket session) nil))
  (ff:free-cstruct (udp-session-address-buffer session)))

(defun set-socket-receive-buffer-size (socket size)
  (let ((value-buffer (ff:malloc-cstruct 'c-int))
	(level #-linux #xffff #+linux 1);SOL_SOCKET
	(so_rcvbuf #-linux #x1002 #+linux 8));SO_RCVBUF
    (do ((size size (ash size -1)))
	((< size 1024))
      (setf (c-int value-buffer) size)
      (let ((result (setsockopt socket level so_rcvbuf value-buffer 4)))
	(if (zerop result)
	    (return size)
	  (warn "setsockopt(SO_RCVBUF,~D) => ~D"
		  size
		  result))))))

(defun send-packet-to (session packet remote-address &key (start 0) (end (length packet)))
  (assert (zerop start))
  (unless (= (sendto
	      (udp-session-socket session)
	      packet (- end start)
	      0
	      (network-address-cstruct remote-address)
	      (network-address-length remote-address))
	     (- end start))
    (unix-error 'udp-send-failed :packet packet)))

(defun receive-packet (session &optional error-p)
  (let ((buffer (udp-session-packet-buffer session))
	(length-buffer (udp-session-length-buffer session))
	(remote-address-buffer (udp-session-address-buffer session)))
    (setf (c-int length-buffer)
      (network-address-length (udp-session-remote-address session)))
    (let ((length
	   (recvfrom
	    (udp-session-socket session)
	    buffer (length buffer)
	    0
	    remote-address-buffer
	    length-buffer)))
      (if (< length 0)
	  (and error-p (unix-error 'udp-receive-failed))
	(let ((ipaddr (si:memref-int
		       remote-address-buffer 4 0 :unsigned-long))
	      (port (si:memref-int
		     remote-address-buffer 2 0 :unsigned-word)))
	  (values buffer
		  0
		  length
		  (addr/port->network-address ipaddr port)))))))
);;#+ExCL before 5.0

#+(and allegro (version>= 5 0))

(progn
  
(defun open-udp-socket ()
  (socket:make-socket :type :datagram
		      :format :binary
		      :address-family :internet))

(defun close-udp-session (session)
  (when (udp-session-socket session)
    (close (udp-session-socket session))
    (setf (udp-session-socket session) nil)))

(defun set-socket-receive-buffer-size (socket size)
  (let ((fd (socket:socket-os-fd socket))
	(value-buffer (ff:malloc-cstruct 'c-int))
	(level #-linux #xffff #+linux 1);SOL_SOCKET
	(so_rcvbuf #-linux #x1002 #+linux 8));SO_RCVBUF
    (do ((size size (ash size -1)))
	((< size 1024) nil)
      (setf (c-int value-buffer) size)
      (let ((result (setsockopt fd level so_rcvbuf value-buffer 4)))
	(if (zerop result)
	    (return size)
	  (warn "setsockopt(SO_RCVBUF,~D) => ~D"
		size
		result))))))

(defun send-packet-to (session packet remote-address &key (start 0) (end (length packet)))
  (assert (zerop start))
  (unless (= (socket:send-to
	      (udp-session-socket session)
	      packet (- end start)
	      :remote-host (network-address-ip-address remote-address)
	      :remote-port (network-address-port remote-address))
	     (- end start))
    (unix-error 'udp-send-failed :packet packet)))

(defun receive-packet (session &optional error-p)
  (multiple-value-bind (string length addr port)
      (socket:receive-from (udp-session-socket session)
			   default-udp-packet-buffer-size) 
    (if (not string)
	(and error-p (unix-error 'udp-receive-failed))
      (values string
	      0
	      length
	      (addr/port->network-address addr port)))))

);;#+ExCL 5.0 or later

#+:cmu
(progn
  
(defun open-udp-socket ()
  (unix:unix-socket af-inet sock-dgram 0))

(defun close-udp-session (session)
  (when (udp-session-remote-address session)
    (free-network-address (udp-session-remote-address session))
    (setf (udp-session-remote-address session) nil))
  (when (udp-session-socket session)
    (unix:unix-close (udp-session-socket session))
    (setf (udp-session-socket session) nil)
    (free-packet-buffer-sap (udp-session-sap-buffer session))
    (free-int-sap (udp-session-length-buffer session))
    (free-sockaddr-in-sap (udp-session-address-buffer session))))

(alien:def-alien-routine ("recvfrom" unix-recvfrom)
    c-call:int
  (socket c-call:int)
  (buffer (* c-call:char))
  (length c-call:int)
  (flags c-call:int)
  (from-address (* c-call:char))
  (from-length (* c-call:int)))

(alien:def-alien-routine ("sendto" unix-sendto)
    c-call:int
  (socket c-call:int)
  (buffer (* c-call:char))
  (length c-call:int)
  (flags c-call:int)
  (from-address (* c-call:char))
  (from-length c-call:int))

(alien:def-alien-routine ("setsockopt" unix-setsockopt)
    c-call:int
  (socket c-call:int)
  (level c-call:int)
  (optname c-call:int)
  (optval (* c-call:char))
  (optlen c-call:int))

(defun send-packet-to (session packet remote-address &key (start 0) (end (length packet)))
  (assert (<= (- end start) 8192))
  (let ((sap-buffer (udp-session-sap-buffer session)))
    (dotimes (index (- end start))
      (setf (system:sap-ref-8 sap-buffer index) (aref packet (+ start index))))
    (unless (= (unix-sendto
		(udp-session-socket session)
		sap-buffer (- end start)
		0
		(network-address-address-buffer remote-address)
		16)
	       (- end start))
      (unix-error 'udp-send-failed :packet packet))))

(defun receive-packet (session)
  (let ((buffer (udp-session-packet-buffer session))
	(length-buffer (udp-session-length-buffer session))
	(address-buffer (udp-session-address-buffer session)))
    (setf (system:sap-ref-32 length-buffer 0) 16)
    (let ((length
	   (unix-recvfrom
	    (udp-session-socket session)
	    (udp-session-sap-buffer session)
	    (length buffer)
	    0
	    address-buffer
	    length-buffer)))
      (and (>= length 0)
	   (progn
	     (dotimes (index length)
	       (setf (aref buffer index)
		 (system:sap-ref-8 (udp-session-sap-buffer session) index)))
	     (let ((ipaddr (ext:ntohl (system:sap-ref-32 address-buffer 4)))
		   (port (ext:ntohs (system:sap-ref-16 address-buffer 2))))
	       (values buffer
		       0
		       length
		       (addr/port->network-address ipaddr port))))))))

(defun set-socket-receive-buffer-size (socket size)
  (let ((value-buffer (get-int-sap))
	(level #-linux #xffff #+linux 1);SOL_SOCKET
	(so_rcvbuf #-linux #x1002 #+linux 8));SO_RCVBUF
    (setf (system:sap-ref-32 value-buffer 0) size)
    (let ((result (unix-setsockopt socket level so_rcvbuf value-buffer 4)))
      (unless (zerop result)
	(warn "setsockopt(SO_RCVBUF,~D) => ~D"
	      size
	      result)))))

);;#+:cmu

#+:sbcl
(progn

(defun open-udp-socket ()
  (make-instance 'sb-bsd-sockets:inet-socket
    :type :datagram
    :protocol (sb-bsd-sockets:get-protocol-by-name "udp")))

(defun close-udp-session (session)
  (when (udp-session-socket session)
    (sb-bsd-sockets:socket-close (udp-session-socket session))
    (setf (udp-session-socket session) nil)))

(defun set-socket-receive-buffer-size (s size)
  (setf (sb-bsd-sockets:sockopt-receive-buffer s)
    size))

(defun receive-packet (session &optional error-p)
  (let ((buffer (udp-session-packet-buffer session)))
    (multiple-value-bind (buf length ipaddr port)
	(sb-bsd-sockets:socket-receive
	 (udp-session-socket session)
	 buffer
	 (length buffer)
	 :element-type '(unsigned-byte 8))
      (values buffer 0 length (addr/port->network-address ipaddr port)))))

(defun send-packet-to (session packet address
		       &key (start 0) (end (length packet)))
  (let ((dest-sockaddr
	 (sb-bsd-sockets::make-sockaddr-for
	  (udp-session-socket session)
	  nil
	  (network-address-ip-address address)
	  (network-address-port address))))
    (sb-bsd-sockets:socket-sendto
     (udp-session-socket session)
     packet
     dest-sockaddr
     :start start
     :end end)))

)

#-Genera
(define-condition udp-send-failed (unix-error)
  ((packet :initarg :packet :reader udp-send-failed-packet))
  (:report (lambda (c stream)
	     (format stream "UDP send failed: ~A" (unix-error-message c)))))

#-Genera
(define-condition udp-receive-failed (unix-error)
  ((packet :initarg :packet :reader udp-send-failed-packet))
  (:report (lambda (c stream)
	     (format stream "UDP receive failed: ~A" (unix-error-message c)))))
