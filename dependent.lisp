;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File Name:	  dependent.lisp
;;; Description:  System-dependent definitions
;;; Author:	  Simon Leinen (simon@liasg3)
;;; Date Created: 22-Feb-94
;;; RCS $Header: /home/leinen/CVS/lisp-snmp/dependent.lisp,v 1.6 2004/01/01 09:29:40 leinen Exp $
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :snmp)

#+(and :allegro (or :svr4 :linux) (version>= 4 2))
(pushnew :foreign-load-so *features*)

#-(or :cmu :lucid)
(pushnew :condition-multiple-inheritance *features*)

;;; DEFINE-LOAD-FORM-MAKER
;;;
;;; The purpose of this macro is to shield the rest of the code from
;;; the difference in signatures of MAKE-LOAD-FORM between
;;; implementations.  X3J13 has changed the draft ANSI standard
;;; definition of MAKE-LOAD-FORM to accept an (optional) second
;;; argument containing the environment.  If your Lisp implements this
;;; new behavior, you need the first definition, if not, use the
;;; second.
;;;
(defmacro define-load-form-maker ((binding) &body body)
  `(defmethod make-load-form (,binding &optional .environment.)
     (declare (ignore .environment.))
     ,@body))

(defparameter default-mib-pathname
    #-asdf #p"sysman:src;objects.defs"
    #+asdf #p"/usr/share/common-lisp/source/sysman/objects.defs")
