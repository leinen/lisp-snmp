;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File Name:	  read-mib.lisp
;;; Description:  Read a MIB in the Compact ISODE-Style Representation
;;; Author:	  Simon Leinen (simon@lia.di.epfl.ch)
;;; Date Created:  3-Nov-93
;;; RCS $Header: /home/leinen/CVS/lisp-snmp/mib/read.lisp,v 1.5 2002/08/11 21:54:29 leinen Exp $  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :snmp)

(defun read-mib (pathname &key name pretty-name mib)
  (unless mib
    (setq mib (make-mib :source pathname
			:name name
			:pretty-name pretty-name
			:default-oid name)))
  (let ((defs
	    (with-open-file (source pathname)
	      (read-mib-defs-from-stream source))))
    (setf (mib-def-count mib) (length defs))
    (finish-mib mib defs)))

;;; FINISH-MIB mib
;;;
;;; Add lookup tables for the definitions in the MIB.
;;;
(defun finish-mib (mib defs)
  (let ((name-to-id-table (make-hash-table :size (length defs)
					   :test #'equal)))
    (dolist (def defs)
      (setf (gethash (first def) name-to-id-table)
	(second def)))
    (setf (mib-name-to-id-table mib) name-to-id-table))
  (dolist (def defs)
    (let ((name (first def))
	  ;; bind this variable so that we are reminded that it isn't used yet.
	  (variable-description (cddr def)))
      (setf (mib-id-tree mib)
	(add-node-with-values-to-id-tree 
	 (mib-encode-oid mib (list name))
	 (list name)
	 (mib-id-tree mib)))))
  (when (typep (mib-default-oid mib) 'string)
    (setf (mib-default-oid mib)
      (mib-encode-oid mib (list (mib-default-oid mib)))))
  mib)

(defun read-mib-defs-from-stream (source)
  (let ((mib '()))
    (labels
	((read-token (source)
	   (skip-whitespace source)
	   (let ((chars '()))
	     (loop
	       (let ((next-char (peek-char nil source nil nil)))
		 (when (whitespace-or-eof-p next-char)
		   (return (coerce (nreverse chars) 'string)))
		 (push (read-char source) chars)))))
	 (read-oid (source)
	   (skip-whitespace source)
	   (let ((subids '())
		 (chars '()))
	     (loop
	       (let ((next-char (peek-char nil source nil nil)))
		 (cond ((whitespace-or-eof-p next-char)
			(return (nreverse (cons (make-subid (nreverse chars))
						subids))))
		       ((char= next-char #\.)
			(read-char source)
			(push (make-subid (nreverse chars)) subids)
			(setq chars '()))
		       (t (push (read-char source) chars)))))))
	 (make-subid (chars)
	   (let ((string (coerce chars 'string)))
	     (assert (/= (length string) 0))
	     (if (digit-char-p (schar string 0))
		 (with-input-from-string (s string)
		   (let ((result (read s)))
		     (check-type result (integer 0))
		     result))
	       string)))
	 (whitespace-or-eof-p (character)
	   (or (not character)
	       (member character '(#\Space #\Tab #\Page))))
	 (skip-whitespace (source)
	   (do ()
	       ((not (whitespace-or-eof-p (peek-char nil source nil nil))))
	     (read-char source)))
	 (parse-line (line)
	   (with-input-from-string (buffer line)
	     (let* ((object-name (read-token buffer))
		    (object-id (read-oid buffer)))
	       ;;(skip-whitespace buffer)
	       (push (list object-name object-id (read-line buffer nil nil))
		     mib)))))
      (loop
	(let ((line (read-line source nil nil nil)))
	  (unless line (return (nreverse mib)))
	  (unless (or (zerop (length line))
		      (and (>= (length line) 2)
			   (string= (subseq line 0 2) "--")))
	    (parse-line line)))))))
