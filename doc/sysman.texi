\input texinfo @c -*- mode: Texinfo -*-
@c @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@c  File Name:	  sysman.texi
@c  Description:  Texinfo Documentation of the SYSMAN Library
@c  Author:	  Simon Leinen (simon@switch.ch)
@c  Date Created: 30-Oct-93
@c  RCS $Header: /home/leinen/CVS/lisp-snmp/doc/sysman.texi,v 1.16 2000/04/08 20:17:35 simon Exp $  
@c @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

@comment %**start of header
@setfilename snmp.info
@settitle SYSMAN

@iftex

@c A4 version
@afourpaper

@syncodeindex fn cp
@syncodeindex vr cp
@syncodeindex tp cp
@end iftex
@ifinfo
@synindex fn cp
@synindex vr cp
@synindex tp cp
@end ifinfo
@comment %**end of header

@titlepage
@title SYSMAN
@subtitle A System Management Substrate for Common Lisp
@author Simon Leinen @code{<simon@@switch.ch>}
@end titlepage

@node Top, , , (dir)

@menu
* Introduction::
* ASN.1::
* SNMP API::
* Agent::
* Projects::
* Index::
@end menu

@node Introduction, SNMP API, , Top
@chapter Introduction
@c ********************************************************************

This manual documents a Common Lisp library for remote system and
network management.  It uses the Simple Network Management Protocol
(SNMP), version 1 (@cite{RFC1157}) to request status information from
remote devices.

I had started to implemented support for version 2 (with the now
historic party-based security model) of the Simple Network Management
Protocol (SNMPv2p).  Now that SNMPv3 has settled down, it would be nice
to implement that instead.

@node SNMP API, Agent, Introduction, Top
@chapter The SNMP API
@c ********************************************************************



@vindex @code{SNMP} package
All symbols documented in this chapter are exported from the
@code{SNMP} package.

All @var{session} arguments can either be strings or
@code{snmp-session}s.  A string argument is interpreted as a host name
and automatically converted into a UDP SNMP session with the given host,
using the standard UDP SNMP port.

@defun snmp-get session attributes
@end defun

Send SNMP a get request for @var{attributes} to @var{session}.  The
@var{attributes} should be a list of OIDs.  If the request succeeds, the
result will be a list of two-element lists that maps the given OIDs to
the received values.  Example:

@example
* (snmp-get "liasg5" '([sysUpTime.0]))

(([system.sysUpTime.0] #<21:28:37.12>))
@end example

@defun snmp-map-table session columns mapfn
@end defun

The @var{columns} argument should be a list of OIDs which define table
columns.  The function walks the corresponding table(s) at the SNMP
agent defined by @var{session}.  For each table row, function
@var{mapfn} is called with the arguments @var{index} and
@var{value}@dots{}.  @var{Index} is a list of integers representing the
table row index, and the @var{value}s are the values for each column in
the row.  If no value is present for a given column in the current row,
@code{NIL} is passed as the corresponding @var{value} argument.

Example:

@example
* (snmp-map-table "private@@localhost" '([tcpConnState]) #'(lambda (index conn-state) (print (list index conn-state))))

((0 0 0 0 22 0 0 0 0 0) 2) 
((0 0 0 0 1024 0 0 0 0 0) 2) 
((0 0 0 0 1025 0 0 0 0 0) 2) 
((0 0 0 0 6000 0 0 0 0 0) 2) 
((127 0 0 1 1029 127 0 0 2 1025) 5) 
((127 0 0 1 1030 127 0 0 2 1025) 5) 
((127 0 0 2 1025 127 0 0 1 1029) 5) 
((127 0 0 2 1025 127 0 0 1 1030) 5) 
NIL
@end example

@defun snmp-set session attributes
@end defun

@defun snmp-request session request-code bindings
@end defun

@defun decode-pdu pdu [:start start] [:end end]

Decodes an SNMP PDU into an ASN.1 object.
@end defun

@defun encode-pdu pdu

Encodes an SNMP object into a BER-encoded PDU.
@end defun

@defvar *mib*
@end defvar

The current MIB (Management Information Base), in an internal format.
This is mainly used to read and print object IDs in symbolic format.

The agent (for Lisp Machines) also uses this data structure to map
incoming OIDs to the functions that manipulate the associated data.

@defun print-oid-with-mib
@end defun

@defvar *snmp-readtable*

This is a variant of the standard Common Lisp readtable that has one
additional read macro defined for the character @code{#\[}.  The read
macro reads an object identifier terminated by @code{#\]} and returns
the corresponding @code{object-id}.  If @code{*mib*} is bound to a
Management Information Base, the read macro will recognize the symbols
in this MIB.

@example
SNMP(43): [sysContact.0]
#<OBJECT-ID system.sysContact.0>
@end example
@end defvar

@node Agent, ASN.1, SNMP API, Top
@chapter The SNMP Agent
@c ********************************************************************

The package includes a rudimentary SNMP agent for Lisp Machines running
the Genera operating system.  In this version, it implements a large
subset of the MIB-II, but only for reading variables.

@node ASN.1, Projects, Agent, Top
@chapter ASN.1 and BER
@c ********************************************************************

Because SNMP is based on ASN.1 (Abstract Syntax Notation One), the
library contains functions for constructing and accessing ASN.1 objects,
as well as encoding and decoding them using BER (the Basic Encoding
Rules).

@vindex @code{ASN.1} package
All symbols documented in this chapter are exported from the
@code{ASN.1} package.

@deftp Type object-id

A type representing an ASN.1 object identifier (OID).
@end deftp

@defun make-object-id subids

Returns an object identifier consisting of the sub-identifiers in the
list @var{subids}.
@end defun

@defun object-id-subids oid

Returns the sub-identifiers of @var{oid} as a list.
@end defun

@defun oid-prefix-p oid1 oid2

Returns true iff @var{oid1} is a prefix of @var{oid2}.
@end defun

@defun list-prefix-p list1 list2

Returns true iff @var{list1} is a prefix of @var{list2}.  The components
of @var{list1} and @var{list2} must be integers permitted in object
IDs.  If the first result is true, a second value is returned that is
the part of @var{list2} which is not the common prefix @var{list1}.
@end defun

@deftp Type typed-asn-tuple

Data type representing an ASN.1 tuple marked with a special type.  These
objects are often used to encode different types of PDUs.
@end deftp

@defun make-typed-asn-tuple type elements

Returns a @code{typed-asn-tuple} of type @var{type} containing the
@var{elements}.
@end defun

@defun typed-asn-tuple-type tuple
@end defun

@defun typed-asn-tuple-elements tuple
@end defun

@section BER Encoding and Decoding

@defun ber-decode pdu [:start start] [:end end]
@end defun

@defun ber-encode item

Encodes @var{item} using BER and returns a byte vector of type
@code{(simple-array (unsigned-byte 8) (*))}.
@end defun

@defvar *application-specific-ber-decoder*

Hook for decoding application-specific BER sequences.

This variable should be bound around calls to @code{ber-decode} and
@code{asn-read}.  Its value must be a function taking three arguments: a
@var{buffer} from which it can read BER octets, @var{type} (with the
@code{asn-application} flag removed) and @var{length}.  It should return
whatever application-specific object is encoded that way.
@end defvar

@defvar *application-specific-ber-encoder*

Hook for BER-encoding application-specific objects.

This variable should be bound around calls to @code{ber-encode} and
@code{asn-write}.  Its value must be a function taking two arguments:
a @var{buffer} to which it can write BER octets, and the @var{object} to
encode.

If your application uses several different types of objects that are
encoded, it makes sense to use a generic function as the value of
@code{*application-specific-ber-encoder*}.  You can then write a
separate encoding method for each type.
@end defvar

The following functions and macros are a lower level interface to BER
encoding and decoding.  In most cases the simpler front-ends
@code{ber-encode} and @code{ber-decode} should be sufficient.

@defmac with-input-from-ber-buffer (ber-buffer pdu [:start start] [:end end]) forms@dots{}
@end defmac

@defun ber-read buffer
@end defun

@defmac with-output-to-ber-buffer (ber-buffer) forms@dots{}
@end defmac

@defun ber-write buffer item
@end defun

@defvar ber-context
@end defvar

@defvar ber-constructor
@end defvar

@defvar ber-application
@end defvar

@defun ber-read-int-1 buffer type length
@end defun

@defun ber-read-octet-string-1 buffer type length
@end defun

@defun ber-write-int buffer int [type]

Write the BER-encoded @var{int} to @var{buffer}.  You can give a
@var{type} to override the standard @code{ber-int-tag}.
@end defun

@defun ber-write-octet-string buffer array [type]

Write the BER-encoded @var{array} to @var{buffer}.  You can give a
@var{type} to override the standard @code{ber-octet-string-tag}.
@var{array} must be of type @code{(simple-array (unsigned-byte 8) (*))}.
@end defun

@node Projects, Index, ASN.1, Top
@appendix Suggestions for Future Enhancements

@subheading ASN.1

@subsubheading More efficient BER decoding (and encoding?)

Something that might be worth improving is the amount of consage
generated when an SNMP PDU is decoded.  Currently the whole PDU is
decoded into a nested list structure.  It would be possible to decode
the PDU into several variables representing a fixed expected structure.
The decoding code would signal a condition if this structure isn't
matched by the PDU.

The neatest interface to such a facility would probably be a macro that
takes a nested variable list, similar to @code{destructuring-bind}, but
with additional information about expected BER @emph{type tags}.  With
such an interface, it would not be necessary to define special-purpose
decoding functions.

It might not be convenient to decode the whole PDU into all
subcomponents at once, because one function may only care about the
outer parts and pass the inner parts to a different function without
wanting to know about it (of course I'm thinking about the
``administrative wrapper'' vs.@ the PDU in SNMP requests).  The macro
should provide the possibility to partially parse a PDU and return an
opaque ``parsing continuation'' as a variable.  This can internally be
represented by the @code{ber-input-buffer} structure.

It is not completely clear to me how @emph{encoding} could be sped up,
but maybe this could be done in a similar way.

In addition, the BER encoder could cache previously generated PDUs and
reuse them when equally (or similarly?) structured data has to be
encoded.

@subsubheading More powerful MIB parsing

The current compiled-MIB parser should be replaced by something that
resembles a real MIB compiler.  Ideally, one should be able to take a
MIB from an RFC, add Lisp code to it and compile it into an agent.

Better MIB parsing would also make it possible to write a MIB browser,
whatever this may be good for.

@subheading SNMP Protocol

@subsubheading Compatibility with SNMPv2c and SNMPv3

Here is what has already been done:

@enumerate
@item
Created a new class @code{snmpv2c-session} which is a subclass of
@code{snmpxc-session}.  The former @code{snmp-session} is now called
@code{snmpv1-session} and is another subclass of the new abstract
@code{snmpxc-session}.  @code{Snmpxc-session} should be used as a base
class for all versions of SNMP with a community-based security model.
@end enumerate

And this is what must still be written:

@enumerate

@item
Implement an SNMPv3 session class and other data structures according to
the new abstract model, such as user-based security information and
view-based access control objects.

@item
Implementation of authentication and privacy protocols.  Consult
@cite{RFC 1321} for a specification of MD5, which is important for
authentication.  The standard privacy protocol is based on DES and
should be implemented outside the United States to avoid problems with
ITAR export restrictions.

@item
Teach the agent to respond to SNMPv2c/v3 queries with SNMPv2c/v3
responses.

@item
@code{get-bulk} in the agent

@item
Use @code{get-bulk} in @code{snmp-get-tree} if the @var{session}
argument is an SNMPv2 session.

@end enumerate

@subsubheading Implement traps

An interface to sending traps is relatively easy to design---all that is
needed is a function @code{snmp-send-trap} which takes a trap PDU.

The interface for trap receipt is more difficult.  My idea is to define
a function @code{(wait-for-snmp-traps @var{callback} [@var{timeout}]}
that implements and endless loop (with optional timeout) waiting for
traps.  Whenever a trap PDU is received, it is (decoded and) handed to
the user-supplied callback function.  In order for this to be useful,
one would need a multitasking Lisp.  But this interface is much easier
to use than one which works under single-tasking.  For CMU CL, one could
write a variant of this using CMUCL-style asynchronously woken-up
functions.

@subheading SNMP Agent

@subsubheading Implement @code{set}

MIB variables need a @code{set} method to allow remote control of the
LISPMs.  At this point, some security should probably be implemented, at
least get/set privileges and MIB views bound to special communities.
This should be done in a way that allows for easy migration to better
security models such as SNMPv3's USM.

@subsubheading Support for other MIBs in the Lisp Machine agent

MIB-II is now almost completely implemented, at least for reading.
However, there are other interesting MIBs that are not.  For example
@cite{RFC 1514}, the Host Resources MIB.  It includes higher-level
information about the operating system, disks, memory, programs and
processes than the @samp{system} section of MIB-II.  If you want to do
real distributed @emph{system} management as opposed to network
management, this will be very interesting.

@subsubheading LISPM MIB

A LISPM-specific MIB should be defined and implemented.  I would think
of something like this:

@example
Symbolics = @{ enterprises ? @}
  lispM = @{ Symbolics 1 @}
    lispMType
    lispMUser
    lispMFep
      lispMFepVersion
    lispMMemory
      lispMMemoryLastFullGC
      lispMMemoryNextFullGC (settable!)
      lispMMemoryTotalAllocated
      lispMMemoryTotalUsed
      lispMMemoryAreaTable
        lispMMemoryAreaEntry
          lispMMemoryAreaIndex
          lispMMemoryAreaEntryName
          lispMMemoryAreaRegions
          lispMMemoryAreaAllocated
          lispMMemoryAreaUsed
    ...
@end example

@subheading Management Applications

Even with library in its somewhat rudimentary current state, it is
possible to write management applications that survey the state of
manageable remote devices on the network.  Using a user interface
toolkit such as Garnet, CLUE or CLIM, it would be a nice project to
implement the ``classical'' network visualization tools that draw a
visual representation of a network, with nodes changing colors depending
on their observed state.

@subheading Ports to other systems

Currently the library only works under recent versions of Allegro, CMU
Common Lisp and Genera.  It is very straightforward to port to other
Lisp dialects that resemble the proposed ANSI standard or CLtL2 and that
have some low-level facilities for TCP/IP, in particular UDP.

If the Lisp doesn't have these low-level facilities itself, it is
usually possible to implement them using foreign functions from the C
library.  The Allegro-specific code is an example of how to do this with
a socket-based C library.  If you are running Unix and have networking,
it is highly probable that your C library contains all the functions
necessary.  Unfortunately the Lisp functions necessary to load these
foreign functions are different between Lisp implementations.

As soon as a Lisp-based coffee machine hits the market, I'll personally
port the SNMP agent to it, promised!

@node Index, , Projects, Top
@unnumbered Index

@printindex cp

@contents

@bye
