;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File Name:	  mib-2.lisp
;;; Description:  Load the MIB
;;; Author:	  Simon Leinen (simon@liasun2)
;;; Date Created:  2-Jun-92
;;; RCS $Header: /home/leinen/CVS/lisp-snmp/mib-2.lisp,v 1.4 2002/08/11 21:50:22 leinen Exp $  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :snmp)

(setq *mib* '#.(read-mib default-mib-pathname
			 :name "mib-2"
			 :pretty-name "MIB-II (RFC 1213)"))
