;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File Name:	  ber-decode.lisp
;;; Description:  BER Decoding of PDUs
;;; Author:	  Simon Leinen (simon@liasun1)
;;; Date Created: 28-May-92
;;; RCS $Header: /home/leinen/CVS/lisp-snmp/ber/decode.lisp,v 1.12 2002/08/11 21:54:14 leinen Exp $  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :asn.1)

(defstruct (ber-input-buffer
	    (:predicate nil)
	    (:copier nil)
	    (:constructor make-ber-input-buffer
			  (byte-array position end
				      &aux (limit (or end (length byte-array)))))
	    (:print-function (lambda (buffer stream level)
			       (declare (type stream stream)
					(ignore level))
			       (print-unreadable-object (buffer stream
							 :type t
							 :identity t))
			       (format stream "@~D[~D]"
				       (ber-input-buffer-position buffer)
				       (ber-input-buffer-limit buffer)))))
  (byte-array	nil
		:type (simple-array (unsigned-byte 8) (*))
		:read-only t)
  (position	0
		:type ber-buffer-pointer)
  (limit	(length byte-array)
		:type ber-buffer-pointer
		:read-only t)
  (end))

(defmacro with-input-from-ber-buffer ((ber-buffer pdu &key (start 0) (end nil))
				      &body body)
  `(let ((,ber-buffer (make-ber-input-buffer ,pdu ,start ,end)))
     ,@body))

(declaim
 (ftype (function (ber-input-buffer) (unsigned-byte 8)) ber-read-byte)
 (ftype (function (ber-input-buffer) ber-length)
	ber-read-length)
 (ftype (function (ber-input-buffer) t) ber-read)
 (ftype (function (ber-input-buffer ber-tag ber-length) t)
	ber-read-application-type-1)
 (ftype (function (ber-input-buffer) (values ber-tag ber-length))
	ber-read-header)
 (ftype (function (ber-input-buffer ber-tag ber-length) (values t ber-tag))
	ber-read-boolean-1)
 (ftype (function (ber-input-buffer ber-tag ber-length) (values array ber-tag))
	ber-read-bit-string-1)
 (ftype (function (ber-input-buffer ber-tag ber-length) (values string ber-tag))
	ber-read-octet-string-1)
 (ftype (function (ber-input-buffer ber-tag ber-length) (values integer ber-tag))
	ber-read-int-1)
 (ftype (function (ber-input-buffer ber-tag ber-length) (values null ber-tag))
	ber-read-null-1)
 (ftype (function (ber-input-buffer ber-tag ber-length)
		  t)
	ber-read-sequence-1)
 (ftype (function (ber-input-buffer ber-tag ber-length)
		  object-id)
	ber-read-object-id-1)
 (ftype (function (ber-input-buffer) oid-component)
	ber-read-subid))

(defun ber-read-byte (buffer)
  (when (>= (ber-input-buffer-position buffer)
	    (ber-input-buffer-limit buffer))
    (error 'ber-message-ended-prematurely))
  (prog1
      (aref (ber-input-buffer-byte-array buffer)
	    (ber-input-buffer-position buffer))
    (incf (ber-input-buffer-position buffer))))

(defun ber-read-length (buffer)
  (let ((byte (ber-read-byte buffer)))
    (if (< byte 128)
	byte
      (let ((nbytes (logandc2 byte ber-long-len)))
	(when (= nbytes 0)
	  (error 'indefinite-length-not-supported))
	(let ((length 0))
	  (declare (type ber-length length))
	  (dotimes (count nbytes length)
	    (setq length (+ (the ber-length (ash length 8))
			    (ber-read-byte buffer)))))))))

(defun ber-read (buffer)
  (multiple-value-bind (type length)
      (ber-read-header buffer)
    (cond ((/= (logand type ber-constructor) 0)
	   (ber-read-sequence-1 buffer type length))
	  ((/= (logand type ber-application) 0)
	   (funcall (the function *application-specific-ber-decoder*)
		    buffer (logandc2 type ber-application) length))
	  ((/= (logand type ber-context) 0)
	   ;;
	   ;; for context types, save the current state so that
	   ;; context-dependent objects can be decoded later using
	   ;; CONTEXT-TYPEP and DECODE-FROM-CONTEXT.
	   ;;
	   (multiple-value-prog1
	       (values (list buffer
			     (ber-input-buffer-position buffer)
			     (ber-input-buffer-limit buffer)
			     type)
		       type)
	     (incf (ber-input-buffer-position buffer) length)))
	  (t (ecase (logand type #x1f)
	       ((#.ber-boolean-tag) (ber-read-boolean-1 buffer type length))
	       ((#.ber-int-tag) (ber-read-int-1 buffer type length))
	       ((#.ber-bit-string-tag) (ber-read-bit-string-1 buffer type length))
	       ((#.ber-octet-string-tag) (ber-read-octet-string-1 buffer type length))
	       ((#.ber-null-tag) (ber-read-null-1 buffer type length))
	       ((#.ber-object-id-tag) (ber-read-object-id-1 buffer type length))
	       ((#.ber-sequence-tag) (ber-read-sequence-1 buffer type length))
	       #+ASN1-SET-SUPPORT
	       ((#.ber-set-tag) (ber-read-set-1 buffer type length)))))))

(defun ber-read-application-type-1 (buffer type length)
  (let ((bytes '()))
    (dotimes (index length)
      (push (ber-read-byte buffer) bytes))
    (apply #'vector type (nreverse bytes))))

(defun ber-read-header (buffer)
  (values (ber-read-byte buffer) (ber-read-length buffer)))

(defun ber-read-boolean-1 (buffer type length)
  (values (/= (ber-read-int-1 buffer type length) 0) type))

(defun ber-read-bit-string-1 (buffer type length)
  (let* ((pad-length (ber-read-byte buffer))
	 (nbytes (- length 1))
	 (nbits (- (* nbytes 8) pad-length))
	 (bit-array (make-array nbits :element-type 'bit)))
    (declare (type ber-length nbytes))
    (assert (<= 0 pad-length 7))
    (dotimes (index (- nbytes 1))
      (declare (type ber-length index))
      (let ((byte (ber-read-byte buffer)))
	(dotimes (bit 8)
	  (setf (aref bit-array (+ (* index 8) bit))
	    (ldb (byte 1 (- 8 bit)) byte)))))
    (let ((byte (ber-read-byte buffer)))
      (dotimes (bit (- 8 pad-length))
	(setf (aref bit-array (+ (* (- nbytes 1) 8) bit))
	  (ldb (byte 1 (- 8 pad-length bit)) byte))))
    (values bit-array type)))

(defun ber-read-octet-string-1 (buffer type length)
  (let ((string (make-string length)))
    (dotimes (index length)
      (setf (schar string index)
	    (code-char (ber-read-byte buffer))))
    (values string type)))

(defun ber-read-int-1 (buffer type length)
  (let ((int 0))
    (dotimes (count length)
      (setq int (+ (ash int 8) (ber-read-byte buffer))))
    (values int type)))

(defun ber-read-null-1 (buffer type length)
  (declare (ignore buffer))
  (assert (= length 0))
  (values nil type))

(defun ber-read-sequence-1 (buffer type length)
  (let ((start (ber-input-buffer-position buffer)))
    (assert (typep start '(integer 0)))
    (let ((sequence
	   (let ((items '()))
	     (loop
	      (case (signum (- (- (ber-input-buffer-position buffer) start)
			       length))
		((1)
		 (error 'ber-decode-error))
		((0)
		 (return (nreverse items)))
		((-1)
		 (push (ber-read buffer) items)))))))
      (if (= (logandc2 type ber-constructor) ber-sequence-tag)
	  sequence
	(make-typed-asn-tuple (logandc2 type ber-constructor) sequence)))))

(defun ber-read-object-id-1 (buffer type length)
  (declare (ignore type))
  (let ((subids '()))
    (let ((start (ber-input-buffer-position buffer)))
      (let ((subids
	     (loop
	       (when (>= (- (ber-input-buffer-position buffer) start) length)
		 (return (nreverse subids)))
	       (push (ber-read-subid buffer) subids))))
	(when subids
	  (let ((subid1 (first subids)))
	    (declare (type oid-component subid1))
	    (when (>= subid1 40)
	      (setq subids (multiple-value-call #'list*
			     (truncate subid1 40) (rest subids))))))
	(make-object-id subids)))))

(defun ber-read-subid (buffer)
  (let ((subid 0))
    (declare (type oid-component subid))
    (loop
      (let ((byte (ber-read-byte buffer)))
	(if (/= (logand byte ber-bit8) 0)
	    (setq subid (+ (ash subid 7) (logandc2 byte ber-bit8)))
	  (return (+ (ash subid 7) byte)))))))

(defun ber-decode (pdu &key (start 0) (end nil))
  (with-input-from-ber-buffer (source pdu :start start :end end)
    (ber-read source)))

(defun context-typep (x &optional context-type)
  (and (consp x)
       (typep (first x) 'ber-input-buffer)
       (typep (second x) 'fixnum)
       (typep (third x) 'fixnum)
       (if context-type
	   (= (fourth x) (logior context-type ber-context))
	   (typep (fourth x) '(unsigned-byte 8)))))

(defun decode-from-context (x)
  (let ((buffer (first x))
	(position (second x)))
    (let ((saved-position (ber-input-buffer-position buffer)))
	(unwind-protect
	     (progn
	       (setf (ber-input-buffer-position buffer) position)
	       (ber-read buffer))
	  (setf (ber-input-buffer-position buffer) saved-position)))))
