;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File Name:	  ber-defs.lisp
;;; Description:  Basic Encoding Rules - Common Definitions
;;; Author:	  Simon Leinen (simon@lia.di.epfl.ch)
;;; Date Created: 30-Oct-93
;;; RCS $Header: /home/leinen/CVS/lisp-snmp/ber/defs.lisp,v 1.9 2002/08/11 21:54:12 leinen Exp $  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :asn.1)

(deftype ber-tag () '(unsigned-byte 8))
(deftype ber-length () '(unsigned-byte 29))

(defconstant ber-boolean-tag		 1)
(defconstant ber-int-tag		 2)
(defconstant ber-bit-string-tag		 3)
(defconstant ber-octet-string-tag	 4)
(defconstant ber-null-tag		 5)
(defconstant ber-object-id-tag		 6)
(defconstant ber-sequence-tag		16)
(defconstant ber-set-tag		17)

(defconstant ber-universal		#x00)
(defconstant ber-application		#x40)
(defconstant ber-context		#x80)
(defconstant ber-private		#xc0)
	
(defconstant ber-primitive		#x00)
(defconstant ber-constructor		#x20)
	
(defconstant ber-long-len		#x80)
(defconstant ber-extension-id		#x1f)
(defconstant ber-bit8			#x80)

(deftype ber-buffer-pointer () '(unsigned-byte 29))

(defvar *application-specific-ber-decoder*)
(defvar *application-specific-ber-encoder*)

(define-condition ber-decode-error (asn-error) ())

(define-condition ber-encode-error (asn-error) ())

(define-condition ber-implementation-restriction 
    (asn-implementation-restriction)
  ())

(define-condition ber-message-ended-prematurely
    (ber-decode-error)
  ()
  (:report (lambda (c stream)
	     (declare (ignore c))
	     (format stream "BER message ended prematurely"))))

(define-condition ber-indefinite-length-not-supported
    (ber-decode-error 
     #+condition-multiple-inheritance asn-implementation-restriction)
  ()
  (:report (lambda (c stream)
	     (declare (ignore c))
	     (format stream "Indefinite length not supported"))))

(define-condition ber-unencodable-length
    (ber-encode-error
     #+condition-multiple-inheritance asn-implementation-restriction)
  ((length :initarg :length
	   :reader ber-unencodable-length-length))
  (:report (lambda (c stream)
	     (format stream "Length ~D cannot be encoded"
		     (ber-unencodable-length-length c)))))
