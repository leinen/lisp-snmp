;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File Name:	  asn1-package.lisp
;;; Description:  Declaration of the ASN.1 Package
;;; Author:	  Simon Leinen (simon@liasun1)
;;; Date Created: 30-May-92
;;; RCS $Header: /home/leinen/CVS/lisp-snmp/asn1/package.lisp,v 1.16 2002/08/11 21:50:55 leinen Exp $  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#-Genera (in-package :cl-user)
#+Genera (in-package :future-common-lisp-user)

(defpackage :asn.1
  (:use #+genera :future-common-lisp
	#-genera :common-lisp)
  #+cmu (:import-from :pcl make-load-form-saving-slots)
  (:export
   ber-context ber-constructor ber-application

   object-id make-object-id object-id-subids
   oid-component
   oid-prefix-p list-prefix-p
   oid-list-< oid-list->=
   typed-asn-tuple make-typed-asn-tuple
   typed-asn-tuple-type typed-asn-tuple-elements
   context-typep decode-from-context

   ber-decode with-input-from-ber-buffer ber-read   
   ber-encode with-output-to-ber-buffer ber-write

   *application-specific-ber-encoder*
   *application-specific-ber-decoder*
   ber-read-int-1 ber-read-octet-string-1
   ber-write-int ber-write-octet-string ber-write-null

   asn-error
   ber-decode-error ber-encode-error
   ber-implementation-restriction))
