;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File Name:	  asn1-defs.lisp
;;; Description:  Common definitions for the ASN.1 package
;;; Author:	  Simon Leinen (simon@liasun1)
;;; Date Created: 28-May-92
;;; RCS $Header: /home/leinen/CVS/lisp-snmp/asn1/defs.lisp,v 1.13 2002/08/11 21:50:52 leinen Exp $  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :asn.1)

(deftype oid-component () '(unsigned-byte 29))
(deftype oid-component-length () '(integer 0 4))

(defstruct (object-id
	    (:copier nil)
	    (:predicate nil)
	    (:constructor make-object-id (subids))
	    (:print-function (lambda (oid stream level)
			       (declare (ignore level))
			       (write-string "[" stream)
			       (snmp:print-oid-with-mib oid stream)
			       (write-string "]" stream))))
  (subids nil :type list :read-only t))

(snmp::define-load-form-maker ((oid object-id))
  `(make-object-id ',(object-id-subids oid)))

(defstruct (typed-asn-tuple
	    (:copier nil)
	    (:constructor make-typed-asn-tuple (type elements))
	    (:print-function (lambda (tuple stream level)
			       (declare (type stream stream)
					(ignore level))
			       (print-unreadable-object (tuple stream :type t)
				 (format stream "[~S] ~{~S~^ ~}"
					 (typed-asn-tuple-type tuple)
					 (typed-asn-tuple-elements tuple))))))
  (type nil :type (unsigned-byte 8) :read-only t)
  (elements nil :type list :read-only t))

(defun list-prefix-p (list1 list2)
  (if (endp list1)
      (values t list2)
    (let ((f1 (first list1)) (f2 (first list2)))
      (declare (type oid-component f1 f2))
      (and (eql f1 f2) (list-prefix-p (rest list1) (rest list2))))))

(defun oid-list->= (oid1 oid2)
  (declare (type list oid1 oid2))
  (or (endp oid2)
      (and (not (endp oid1))
	   (let ((f1 (first oid1)) (f2 (first oid2)))
	     (declare (type oid-component f1 f2))
	     (or (> f1 f2)
		 (and (= f1 f2)
		      (oid-list->= (rest oid1) (rest oid2))))))))

(defun oid-list-< (oid1 oid2)
  (declare (type list oid1 oid2))
  (and (not (endp oid2))
       (or (endp oid1)
	   (let ((f1 (first oid1)) (f2 (first oid2)))
	     (declare (type oid-component f1 f2))
	     (or (< f1 f2)
		 (and (= f1 f2)
		      (oid-list-< (rest oid1) (rest oid2))))))))

(defun oid-prefix-p (oid1 oid2)
  (declare (type object-id oid1 oid2))
  (list-prefix-p (object-id-subids oid1) (object-id-subids oid2)))

;;;; Conditions

(define-condition asn-error (error) ())

(define-condition asn-implementation-restriction (asn-error) ())
