;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File Name:	  sgi-agent.lisp
;;; Description:  Attach to IRIX' Extensible SNMP Agent
;;; Author:	  Simon Leinen (simon@liasg7)
;;; Date Created: 15-Jan-1995
;;; RCS $Header: /home/leinen/CVS/lisp-snmp/agent/sgi/sgi-agent.lisp,v 1.10 2002/08/11 21:53:49 leinen Exp $
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :snmp)

(defvar *sgi-agents* '()
  "List of listening agents")

(defstruct (sgi-agent
	    (:include udp-session)
	    (:copier nil)
	    (:predicate nil)
	    (:constructor make-sgi-agent (name local-host local-port)))
  (name nil :read-only t)
  #+ExCL (handler nil :type (or null mp:process))
  #+CMU (handler nil))

(declaim (ftype (function (sgi-agent) t) close-sgi-agent)
	 (ftype (function (t t (unsigned-byte 16) (or symbol function))
			  (or sgi-agent null))
		add-sgi-agent))

(defun close-sgi-agent (agent)
  (remove-sgi-agent-background-processing agent)
  (close-udp-session agent)
  (setq *sgi-agents*
    (remove agent *sgi-agents*)))

#+CMU
(progn

(defun initialize-sgi-agent-background-processing (agent func)
  ;; On CMU Common Lisp, we can conveniently define an "FD-HANDLER"
  ;; function.  It will be called asynchronously whenever there is
  ;; input pending on the socket.
  #+CMU
  (setf (sgi-agent-handler agent)
    (system:add-fd-handler (udp-session-socket agent) :input 
			   #'(lambda (socket)
			       (declare (ignore socket))
			       (funcall func agent)))))

(defun remove-sgi-agent-background-processing (agent)
  (when (sgi-agent-handler agent)
    (system:remove-fd-handler (sgi-agent-handler agent))))
);;#+CMU

#+excl
(progn

(defun initialize-sgi-agent-background-processing (agent func)
  ;; On Allegro and other systems with real multiprocessing, we start
  ;; a subprocess that waits on the agent's socket.
  (setf (sgi-agent-handler agent)
    (mp:process-run-function
     (format nil "SNMP Request Handler ~S" (sgi-agent-name agent))
     #'(lambda ()
	 (loop
	  (mp:wait-for-input-available
	   (udp-session-socket agent)
	   :whostate "waiting for SNMP request to arrive")
	  (funcall func agent))))))

(defun remove-sgi-agent-background-processing (agent)
  (when (sgi-agent-handler agent)
    (mp:process-kill (sgi-agent-handler agent))))
);;#+ExCL

(defun add-sgi-agent (name local-host local-port func)
  (let ((old-agent-rest (member name *sgi-agents* :key #'sgi-agent-name)))
    (when (not (endp old-agent-rest))
      (close-sgi-agent (first old-agent-rest))))
  (let ((agent (make-sgi-agent name local-host local-port)))
    (initialize-udp-session agent)
    (initialize-sgi-agent-background-processing agent func)
    (push agent *sgi-agents*)
    agent))

(defvar *foo-trace* '())

(defun handle-foo (agent)
  (multiple-value-bind (packet start end return-address)
      (receive-packet agent t)
    (let ((pdu (decode-pdu packet :start start :end end)))
      (agent-respond agent pdu return-address))))

(defun agent-respond (agent pdu return-address)
  (push (cons pdu return-address) *foo-trace*)
  (let ((answer pdu))
    (send-packet-to agent (encode-pdu answer) return-address)))
