;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File Name:	  agent.lisp
;;; Description:  SNMP Agent
;;; Author:	  Simon Leinen (simon@liasun1)
;;; Date Created: 30-May-92
;;; RCS $Header: /home/leinen/CVS/lisp-snmp/agent/generic/main.lisp,v 1.34 2003/12/29 10:12:34 leinen Exp $  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; This used to only work under Genera, but after six and a half
;;; years I generalized it so that it works under Allegro at least.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :snmp)

(defstruct (snmp-agent-state
	    #-Genera
	    (:include udp-session)
	    (:predicate nil)
	    (:copier nil)
	    (:conc-name sas-))
  (start-up-time
   (get-internal-real-time))
  (in-pkts 0 :type (unsigned-byte 32))
  (out-pkts 0 :type (unsigned-byte 32))
  (in-bad-versions 0 :type (unsigned-byte 32))
  (in-bad-community-names 0 :type (unsigned-byte 32))
  (in-bad-community-uses 0 :type (unsigned-byte 32))
  (in-asn-parse-errs 0 :type (unsigned-byte 32))
  (in-too-bigs 0 :type (unsigned-byte 32))
  (in-no-such-names 0 :type (unsigned-byte 32))
  (in-bad-values 0 :type (unsigned-byte 32))
  (in-read-onlys 0 :type (unsigned-byte 32))
  (in-gen-errs 0 :type (unsigned-byte 32))
  (in-total-req-vars 0 :type (unsigned-byte 32))
  (in-total-set-vars 0 :type (unsigned-byte 32))
  (in-get-requests 0 :type (unsigned-byte 32))
  (in-get-nexts 0 :type (unsigned-byte 32))
  (in-set-requests 0 :type (unsigned-byte 32))
  (in-get-responses 0 :type (unsigned-byte 32))
  (in-traps 0 :type (unsigned-byte 32))
  (out-too-bigs 0 :type (unsigned-byte 32))
  (out-no-such-names 0 :type (unsigned-byte 32))
  (out-bad-values 0 :type (unsigned-byte 32))
  (out-gen-errs 0 :type (unsigned-byte 32))
  (out-get-requests 0 :type (unsigned-byte 32))
  (out-get-nexts 0 :type (unsigned-byte 32))
  (out-set-requests 0 :type (unsigned-byte 32))
  (out-get-responses 0 :type (unsigned-byte 32))
  (out-traps 0 :type (unsigned-byte 32))
  (enable-authen-traps nil :type t))

(defstruct (snmpxc-agent-state
	    (:include snmp-agent-state)
	    (:copier nil)
	    (:predicate nil))
  (community-table))

(defstruct (snmpv1/2c-agent-state
	    (:include snmpxc-agent-state)
	    (:copier nil)
	    (:predicate nil))
  (in-get-bulks 0 :type (unsigned-byte 32)))

(defvar *the-snmp-agent*)

(defun close-snmp-agent (agent)
  (close-udp-session agent))

#+Genera
(setq *the-snmp-agent* (make-snmp-agent-state))

#+Genera
(net:define-server :snmp (:medium :datagram
				  :request-array (request start end))
  (let ((agent *the-snmp-agent*)
	(neti:*server-debug-flag* t))
    (multiple-value-bind (return-p response)
	(agent-process-incoming-packet
	 *the-snmp-agent* request start end)
      (when return-p
	(send-response agent response)))))

(defun run-agent (&rest agent-args &key
				   (port default-snmp-udp-port)
				   (backgroundp t)
		  &allow-other-keys)
  (setq agent-args (remprop :port agent-args))
  (setq agent-args (remprop :backgroundp agent-args))
  (let ((agent (apply #'make-snmpv1/2c-agent-state
		      :local-port port
		      agent-args)))
    (initialize-udp-session agent)
    (setq *the-snmp-agent* agent)
    (if backgroundp
	(progn
	  #+allegro
	  (mp:process-run-function
	   "SNMP Agent"
	   #'(lambda () (agent-loop agent :close-on-terminate t)))
	  #-allegro
	  (agent-loop agent :close-on-terminate t))
      (agent-loop agent :close-on-terminate t))))

(defun agent-loop (agent &key close-on-terminate)
  (let ((*the-snmp-agent* agent))
    (unwind-protect
	(loop
	  #+allegro
	  (mp:wait-for-input-available
	   (list (socket:socket-os-fd (sas-socket agent)))
	   :whostate "Waiting for request PDU")
	  (multiple-value-bind (pdu start end return-address)
	      (receive-packet agent t)
	    (multiple-value-bind (respond-p response) 
		(agent-process-incoming-packet agent pdu start end)
	      (when respond-p
		(send-packet-to agent response return-address)))))
      (when close-on-terminate
	(close-snmp-agent agent)))))

(defun agent-process-incoming-packet (agent pdu start end)
  (counter32-incf (sas-in-pkts agent))
  (handler-case
      (let ((request (decode-pdu pdu :start start :end end)))
	(cond ((consp request)
	       (unless (and (integerp (first request))
			    (stringp (second request))
			    (endp (cdddr request)))
		 (error 'snmpv1-malformed-pdu-error
			:request (subseq pdu start end)))
	       (unless (or (= (first request) snmp-version-1)
			   (= (first request) snmp-version-2c))
		 (signal 'snmp-bad-version-error
			 :request pdu
			 :requested-version (first request)))
	       (multiple-value-prog1
		   (snmpxc-reply request agent (first request))
		 (counter32-incf (sas-out-pkts agent))))
	      (t (signal 'snmp-malformed-pdu-error
			 :request pdu))))
    (asn.1:asn-error (c)
      (declare (ignore c))
      (counter32-incf (sas-in-asn-parse-errs agent)) nil)
    (snmp-bad-version-error (c)
      (declare (ignore c))
      (counter32-incf (sas-in-bad-versions agent)) nil)
    (snmp-request-bad-community-error (c)
      (declare (ignore c))
      (counter32-incf (sas-in-bad-community-names agent)) nil)
    (snmp-agent-no-such-name-error (c)
      (counter32-incf (sas-in-no-such-names agent))
      (let ((response (make-error-pdu c)))
	(incf (sas-out-get-responses agent))
	(values t response)))))

(defmethod make-error-pdu ((c snmp-agent-specific-variable-error))
  (let ((request (snmp-agent-request-error-request c)))
    (let ((version (pdu-snmp-version request))
	  (community (pdu-snmp-community request))
	  (type (pdu-type request))
	  (request-id (pdu-request-id request))
	  (bindings (pdu-bindings request)))
    (encode-pdu
     (list version community
	   (make-typed-asn-tuple
	    pdu-type-response
	    (list request-id
		  (snmp-agent-specific-variable-error-error-status c)
		  (snmp-agent-specific-variable-error-variable-index c)
		  bindings)))))))

(defun snmpxc-reply (request agent version)
  (let ((snmp-version (pdu-snmp-version request))
	(community (pdu-snmp-community request))
	(type (pdu-type request))
	(request-id (pdu-request-id request))
	(bindings (pdu-bindings request)))
    (unless (= snmp-version version)
      (signal 'snmp-bad-version-error
	      :request request :requested-version snmp-version))
    (let ((view (snmpxc-community->view community agent)))
      (unless view
	(signal 'snmp-request-bad-community-error
		:request request :community community))
      (let ((new-bindings
	     (snmp-response agent type bindings view version request)))
	(when new-bindings
	  (let ((response (encode-pdu
			   (list version community
				 (make-typed-asn-tuple 
				  pdu-type-response
				  (list request-id 0 0
					new-bindings))))))
	    (incf (sas-out-get-responses agent))
	    (values t response)))))))

(defmethod snmpxc-community->view (community agent)
  (cond ((string= community "public") t)
	(t nil)))

(defun snmp-response (agent type bindings view version request)
  (case type
    ((#.pdu-type-get-request)
     (counter32-incf (sas-in-get-requests agent))
     (new-bindings-for-get-request agent bindings view request))
    ((#.pdu-type-get-next-request)
     (counter32-incf (sas-in-get-nexts agent))
     (new-bindings-for-get-next-request agent bindings view request))
    ((#.pdu-type-get-bulk-request)
     (cond ((= version snmp-version-1)
	    (counter32-incf (sas-in-asn-parse-errs agent))
	    nil)
	   (t (counter32-incf (snmpv1/2c-agent-state-in-get-bulks agent))
	      (new-bindings-for-get-bulk-request agent bindings view request))))
    (otherwise
     (counter32-incf (sas-in-asn-parse-errs agent))
     nil)))

(defun new-bindings-for-get-request (agent bindings view request)
  (sequential-new-bindings
   bindings agent view request
   #'get-response-binding))

(defun new-bindings-for-get-next-request (agent bindings view request)
  (sequential-new-bindings
   bindings agent view request
   #'get-next-response-binding))

(defconstant max-supported-repetitions 10)

(defun new-bindings-for-get-bulk-request (agent bindings view request)
  (let ((non-repeaters (pdu-non-repeaters request))
	(max-repetitions (pdu-max-repetitions request)))
    (when (> max-repetitions max-supported-repetitions)
      (setq max-repetitions max-supported-repetitions))
    (when (< max-repetitions 0)
      (signal 'snmp-get-bulk-illegal-max-repetitions
	      :request request
	      :max-repetitions max-repetitions))
    (when (or (< non-repeaters 0)
	      (> non-repeaters (length bindings)))
      (signal 'snmp-get-bulk-illegal-non-repeaters
	      :request request
	      :non-repeaters non-repeaters))
    (let ((non-repeaters (subseq bindings 0 non-repeaters))
	  (repeaters (subseq bindings non-repeaters)))
      (let ((result '()))
	(setq result (revappend
		      (sequential-new-bindings
		       non-repeaters agent view request
		       #'get-next-response-binding)
		      result))
	(dotimes (i max-repetitions)
	  ;;TODO: Handle termination condition (response packet too big)
	  (let ((new-bindings
		 (sequential-new-bindings
		  repeaters agent view request
		  #'get-next-response-binding)))
	    (setq result (revappend new-bindings result))
	    (setq repeaters new-bindings)
	    (when (every #'(lambda (x)
			     (eq (cadr x) +end-of-mib-view+))
			 new-bindings)
	      (return))))
	(nreverse result)))))

(defun sequential-new-bindings (bindings agent view request mapfn)
  (do ((variable-counter 1 (1+ variable-counter))
       (b bindings (rest b))
       (new-bindings '()))
      ((endp b) (nreverse new-bindings))
    (let ((binding (first b)))
      (push (funcall mapfn binding agent view request variable-counter)
	    new-bindings))))

(defun get-response-binding (binding agent view request variable-counter)
  (list (car binding)
	(or (mib-get-value 
	     *mib*
	     (object-id-subids
	      (first binding))
	     agent
	     view)
	    (signal 'snmp-agent-no-such-name-error
		    :request request
		    :index variable-counter))))

(defun get-next-response-binding (binding agent view request variable-counter)
  (multiple-value-bind
      (oid value)
      (mib-get-next-value
       *mib*
       (object-id-subids
	(first binding))
       agent
       view)
    (if oid
	(list (make-object-id oid) value)
      (if (= (pdu-snmp-version request) snmp-version-1)
	  (signal 'snmp-agent-no-such-name-error
		  :request request
		  :index variable-counter)
	(list (make-object-id oid) +end-of-mib-view+)))))
