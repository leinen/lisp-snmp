;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File Name:	  test.lisp
;;; Description:  SNMP tests
;;; Author:	  Simon Leinen (simon@liasun5)
;;; Date Created: 27-May-92
;;; RCS $Header: /home/leinen/CVS/lisp-snmp/test.lisp,v 1.6 2002/08/11 21:50:25 leinen Exp $  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defpackage :sysman
  (:use :common-lisp :asn.1 :snmp))

(in-package :snmp)

(eval-when (eval compile load)
  (defun generate-host-list (base number)
    (let ((hosts '()))
      (dotimes (index number hosts)
	(push (format nil "~A~D" base (- number index)) hosts)))))

(defparameter testhosts
    (append (generate-host-list "liasun" 11)
	    (generate-host-list "liasg" 4)
	    ;; (generate-host-list "masg" 35)
	    ))

(defun test ()
  (let ((hosts testhosts)
	(attributes '(("sysContact" 0)
		      ("sysUpTime" 0)
		      ("sysLocation" 0))))
    (let ((oids (mapcar #'(lambda (oid)
			    (make-object-id (mib-encode-oid *mib* oid)))
			attributes))
	  (sessions (mapcar #'(lambda (host)
				(open-snmp-session :host host))
			    hosts))
	  (answers '()))
      (unwind-protect
	  (dolist (session sessions (nreverse answers))
	    (push (list (udp-session-remote-host session)
			(snmp-get session oids))
		  answers))
	(mapcar #'close-snmp-session sessions)))))
