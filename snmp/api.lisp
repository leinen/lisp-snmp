;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File Name:	  snmp-session.lisp
;;; Description:  Higher level SNMP functions
;;; Author:	  Simon Leinen (simon@liasun1)
;;; Date Created: 19-Jul-95
;;; RCS $Header: /home/leinen/CVS/lisp-snmp/snmp/api.lisp,v 1.5 2004/01/01 09:33:51 leinen Exp $ 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :snmp)

(declaim
 (ftype (function (string) (values t t))
	string->host/community)
 (ftype (function (snmp-session t (function (t &rest args) t)))
	snmp-map-table))

(defun snmp-get (session attributes)
  (snmp-request
   session pdu-type-get-request
   (mapcar #'(lambda (attribute) (list attribute nil)) attributes)
   t))

(defmethod snmp-map-table ((session snmpv1-session) columns function)
  (let (bindings (index '())
	(columns (mapcar #'object-id-subids columns)))
    (loop
      (handler-case
	  (setq bindings
	    (snmp-request
	     session pdu-type-get-next-request 
	     (mapcar #'(lambda (column)
			 (list (make-object-id (append column index)) nil))
		     columns)))
	(snmp-no-such-name-error (c) (declare (ignore c)) (return))
	(snmp-generic-error (c) (setf (cdar bindings) c) (return)))
      (unless bindings
	(error "no response from SNMP service on ~S" session))
      (let ((min-index '()))
	(if (do ((columns columns (rest columns))
		 (bindings bindings (rest bindings)))
		((endp bindings) (not min-index))
	      (multiple-value-bind (prefix-p index)
		  (list-prefix-p
		   (first columns)
		   (object-id-subids (first (first bindings))))
		(when (and prefix-p (or (not min-index)
					(oid-list-< index min-index)))
		  (setq min-index index))))
	    (return)
	  (progn
	    (apply function
		   min-index
		   (mapcar #'(lambda (binding column)
			       (and (equal (object-id-subids (first binding))
					   (append column min-index))
				    (second binding)))
			   bindings
			   columns))
	    (setq index min-index)))))))

(defmethod snmp-map-table ((session string) columns function)
  (multiple-value-bind (host community)
      (string->host/community session)
    (with-snmp-session (session :remote-host host :snmp-community community)
      (snmp-map-table session columns function))))

(defun string->host/community (string)
  (let ((index (position #\@ string :from-end t :test #'char=)))
    (if index
	(values (subseq string (+ index 1))
		(subseq string 0 index))
      (values string default-snmp-community))))

(defun snmp-get-next (session attributes)
  (snmp-request
   session pdu-type-get-next-request
   (mapcar #'(lambda (attribute) (list attribute nil)) attributes)))

(defun snmp-set (session bindings)
  (snmp-request session pdu-type-set-request bindings t))
