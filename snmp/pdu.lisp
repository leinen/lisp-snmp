;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File Name:	  snmp-pdu.lisp
;;; Description:  BER-encoding and decoding for SNMP PDUs
;;; Author:	  Simon Leinen (simon@liasun1)
;;; Date Created: 28-May-92
;;; RCS $Header: /home/leinen/CVS/lisp-snmp/snmp/pdu.lisp,v 1.24 2003/12/29 10:15:17 leinen Exp $  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :snmp)

(defconstant snmp-version-1	0)
(defconstant snmp-version-2c	1)
(defconstant snmp-port		161)
(defconstant snmp-trap-port	162)

(defparameter default-snmp-community	"public")
(defconstant default-snmp-version	snmp-version-1)

(defconstant pdu-type-get-request	  (logior ber-context #x00))
(defconstant pdu-type-get-next-request	  (logior ber-context #x01))
(defconstant pdu-type-response		  (logior ber-context #x02))
(defconstant pdu-type-set-request	  (logior ber-context #x03))
(defconstant pdu-type-snmpv1-trap-request (logior ber-context #x04));obsolete
(defconstant pdu-type-get-bulk-request	  (logior ber-context #x05))
(defconstant pdu-type-inform-request	  (logior ber-context #x06))
(defconstant pdu-type-snmpv2-trap-request (logior ber-context #x07))

(defvar +no-such-object+)
(defvar +no-such-instance+)
(defvar +end-of-mib-view+)

(defstruct (null-encoded-object
	    (:copier nil)
	    (:predicate nil)
	    (:constructor make-null-encoded-object (ber-type)))
  (ber-type))

(setq +no-such-object+ (make-null-encoded-object (logior ber-context 0)))
(setq +no-such-instance+ (make-null-encoded-object (logior ber-context 1)))
(setq +end-of-mib-view+ (make-null-encoded-object (logior ber-context 2)))

(eval-when (compile load eval)
  (defconstant ip-address-tag		0)
  (defconstant counter32-tag		1)
  (defconstant gauge32-tag		2)
  (defconstant timeticks-tag		3)
  (defconstant opaque-tag		4)
  ;; The following have been introduced by SNMPv2
  (defconstant nsap-address-tag		5)
  (defconstant counter64-tag		6)
  (defconstant uinteger32-tag		7)

  (defconstant error-status-no-error		  0)
  (defconstant error-status-too-big		  1)
  (defconstant error-status-no-such-name	  2);for proxy compatibility
  (defconstant error-status-bad-value		  3);for proxy compatibility
  (defconstant error-status-read-only		  4);for proxy compatibility
  (defconstant error-status-generic-error	  5)
  (defconstant error-status-no-access		  6)
  (defconstant error-status-wrong-type		  7)
  (defconstant error-status-wrong-length	  8)
  (defconstant error-status-wrong-encoding	  9)
  (defconstant error-status-wrong-value		 10)
  (defconstant error-status-no-creation		 11)
  (defconstant error-status-inconsistent-value	 12)
  (defconstant error-status-resource-unavailable 13)
  (defconstant error-status-commit-failed	 14)
  (defconstant error-status-undo-failed		 15)
  (defconstant error-status-authorization-error	 16)
  (defconstant error-status-not-writable	 17)
  (defconstant error-status-inconsistent-name	 18))

(defstruct (timeticks
	    (:copier nil)
	    (:predicate nil)
	    (:constructor make-timeticks (ticks))
	    (:print-function
	     (lambda (ticks stream level)
	       (declare (ignore level))
	       (print-unreadable-object (ticks stream)
		 (multiple-value-bind (seconds hundreths)
		     (truncate (timeticks-ticks ticks) 100)
		   (multiple-value-bind (minutes seconds)
		       (truncate seconds 60)
		     (multiple-value-bind (hours minutes)
			 (truncate minutes 60)
		       (multiple-value-bind (days hours)
			   (truncate hours 24)
			 (format stream "~:[~D+~;~*~]~:[~D:~;~*~]~2,'0D:~2,'0D.~2,'0D"
				 (zerop days) days
				 (and (zerop days) (zerop hours)) hours
				 minutes seconds hundreths)))))))))
  (ticks nil :type (integer 0) :read-only t))

(defstruct (ip-address
	    (:copier nil)
	    (:predicate nil)
	    (:constructor make-ip-address-from-octet-string (octets))
	    (:print-function
	     (lambda (ip-address stream level)
	       (declare (ignore level))
	       (print-unreadable-object (ip-address stream)
		 (format stream "ip-address: ~S" (ip-address-octets ip-address))))))
  (octets nil :type (simple-array (unsigned-byte 8) (*))))

(defun make-ip-address-from-number (ip)
  (make-ip-address-from-list (ip-addr-numeric->list ip)))

(defun ip-addr-numeric->list (ip)
  (labels ((numeric->list-1 (addr count list)
	     (if (zerop count)
		 list
	       (numeric->list-1
		(ash addr -8)
		(1- count)
		(cons (logand #xff addr) list)))))
    (numeric->list-1 ip 4 '())))

(defun make-ip-address-from-list (address)
  (make-ip-address-from-octet-string
   (make-array '(4) 
	       :element-type '(unsigned-byte 8)
	       :initial-contents address)))

(defstruct (counter32
	    (:copier nil)
	    (:predicate nil)
	    (:constructor %make-counter32 (value))
	    (:print-function
	     (lambda (counter stream level)
	       (declare (ignore level))
	       (print-unreadable-object (counter stream)
		 (format stream "counter32: ~D" (counter32-value counter))))))
  (value 0 :type (unsigned-byte 32)))

(defun make-counter32 (value)
  (%make-counter32 (logand value #.(- (ash 1 32) 1))))

(defstruct (counter64
	    (:copier nil)
	    (:predicate nil)
	    (:constructor %make-counter64 (value))
	    (:print-function
	     (lambda (counter stream level)
	       (declare (ignore level))
	       (print-unreadable-object (counter stream)
		 (format stream "counter64: ~D" (counter64-value counter))))))
  (value 0 :type (unsigned-byte 64)))

(defun make-counter64 (value)
  (%make-counter64 (logand value #.(- (ash 1 64) 1))))

(defstruct (gauge32
	    (:copier nil)
	    (:predicate nil)
	    (:constructor make-gauge32 (value))
	    (:print-function
	     (lambda (gauge stream level)
	       (declare (ignore level))
	       (print-unreadable-object (gauge stream)
		 (format stream "gauge32: ~D" (gauge32-value gauge))))))
  (value 0 :type (unsigned-byte 32)))

(defun decode-pdu (pdu &key (start 0) (end nil))
  (let ((*application-specific-ber-decoder* #'ber-decode-snmp-objects))
    (ber-decode pdu :start start :end end)))

(defun encode-pdu (pdu)
  (let ((*application-specific-ber-encoder* #'ber-encode-snmp-objects))
    (ber-encode pdu)))

(defun ber-decode-snmp-objects (buffer type length)
  (ecase type
    ((#.ip-address-tag)
     (ber-read-octet-string-1 buffer type length))
    ((#.counter32-tag #.gauge32-tag)
     (ber-read-int-1 buffer type length))
    ((#.timeticks-tag)
     (make-timeticks (ber-read-int-1 buffer type length)))
    ((#.opaque-tag)
     (ber-read-octet-string-1 buffer type length))
    ((#.uinteger32-tag)
     (ber-read-int-1 buffer type length))
    ((#.counter64-tag)
     (ber-read-int-1 buffer type length))))

(defmethod ber-encode-snmp-objects (buffer (v null-encoded-object))
  (ber-write-null buffer (null-encoded-object-ber-type v)))

(defmethod ber-encode-snmp-objects (buffer (tt timeticks))
  (ber-write-int buffer (timeticks-ticks tt)
		 (logior ber-application #.timeticks-tag)))

(defmethod ber-encode-snmp-objects (buffer (c counter32))
  (ber-write-int buffer 
		 (counter32-value c)
		 (logior ber-application #.counter32-tag)))

(defmethod ber-encode-snmp-objects (buffer (c counter64))
  (ber-write-int buffer 
		 (counter64-value c)
		 (logior ber-application #.counter64-tag)))

(defmethod ber-encode-snmp-objects (buffer (c gauge32))
  (ber-write-int buffer (gauge32-value c)
		 (logior ber-application #.gauge32-tag)))

(defmethod ber-encode-snmp-objects (buffer (a ip-address))
  (ber-write-octet-string buffer (ip-address-octets a)
			  (logior ber-application #.ip-address-tag)))

(defmethod ber-encode-snmp-objects (buffer item)
  (declare (ignore buffer))
  (error "Cannot BER-encode ~S yet" item))

;;; Use whatever you find faster
;;
;;(defun counter32-1+ (counter)
;;  (declare (type (unsigned-byte 32) counter))
;;  (the (unsigned-byte 32)
;;    (mod (1+ counter) #.(expt 2 32))))
(defun counter32-1+ (counter)
  (declare (type (unsigned-byte 32) counter))
  (if (= counter #.(1- (expt 2 32)))
      0
    (the (unsigned-byte 32) (1+ counter))))

(define-modify-macro counter32-incf () counter32-1+)
