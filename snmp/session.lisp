;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File Name:	  snmp-session.lisp
;;; Description:  Session between management application and SNMP agent
;;; Author:	  Simon Leinen (simon@liasun1)
;;; Date Created: 28-May-92
;;; RCS $Header: /home/leinen/CVS/lisp-snmp/snmp/session.lisp,v 1.34 2004/01/01 09:34:00 leinen Exp $  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :snmp)

(defconstant default-timeout 1.0)
(defconstant default-retries 3)

(defstruct (snmp-session
	    (:include udp-session)
	    (:copier nil)
	    (:predicate nil)
	    (:print-function
	     print-snmp-session))
  (request-id (random (expt 2 31)) :type (unsigned-byte 32))
  (timeout default-timeout :type real)
  (retries default-retries :type (integer 0)))

#+Genera
(defun print-snmp-session (session stream level)
  (declare (ignore level))
  (print-unreadable-object (session stream :type t)
    (format stream "~S ~S"
	    (udp-session-remote-host session)
	    (snmp-session-request-id session))))

#-Genera
(defun print-snmp-session (session stream level)
  (declare (ignore level))
  (print-unreadable-object (session stream :type t)
    (format stream "~S ~S ~S"
	    (udp-session-remote-host session)
	    (udp-session-socket session)
	    (snmp-session-request-id session))))

(defstruct (snmpxc-session
	    (:include snmp-session)
	    (:copier nil)
	    (:predicate nil))
  (snmp-community default-snmp-community :type string))

(defstruct (snmpv1-session
	     (:include snmpxc-session)
	     (:copier nil)
	     (:predicate nil)))

(defmethod snmp-session-snmp-version ((s snmpv1-session))
  (declare (ignorable s))
  snmp-version-1)

(defstruct (snmpv2c-session
	     (:include snmpxc-session)
	     (:copier nil)
	     (:predicate nil)))

(defmethod snmp-session-snmp-version ((s snmpv2c-session))
  (declare (ignorable s))
  snmp-version-2c)

(defun snmp-session-next-request-id (session)
  (mod (incf (snmp-session-request-id session))
       (expt 2 31)))

#-Genera
(defconstant default-snmp-udp-port 161)

#-Genera
(defun open-snmp-session (&rest initargs
			  &key (remote-port default-snmp-udp-port)
			  &allow-other-keys)
  (setf (getf initargs :remote-port) remote-port)
  (let ((session (apply #'make-snmpv1-session initargs)))
    (initialize-udp-session session)
    session))

#+Genera
(defun open-snmp-session (&rest initargs
			  &key (remote-service :network-management)
			  &allow-other-keys)
  (setf (getf initargs :remote-service) remote-service)
  (let ((session (apply #'make-snmpv1-session initargs)))
    (initialize-udp-session session)
    session))

(defun close-snmp-session (session)
  (close-udp-session session))

(defmacro with-snmp-session ((session &rest initargs) &body body)
  `(let ((,session (open-snmp-session ,@initargs)))
     (unwind-protect
	 (progn ,@body)
       (close-snmp-session ,session))))

(defun pdu-snmp-version (pdu) (first pdu))
(defun pdu-snmp-community (pdu) (second pdu))
(defun pdu-type (pdu) (typed-asn-tuple-type (third pdu)))
(defun pdu-request-id (pdu) (first (typed-asn-tuple-elements (third pdu))))
(defun pdu-error-status (pdu) (second (typed-asn-tuple-elements (third pdu))))
(defun pdu-error-index (pdu) (third (typed-asn-tuple-elements (third pdu))))
(defun pdu-non-repeaters (pdu) (second (typed-asn-tuple-elements (third pdu))))
(defun pdu-max-repetitions (pdu) (third (typed-asn-tuple-elements (third pdu))))
(defun pdu-bindings (pdu) (fourth (typed-asn-tuple-elements (third pdu))))

(defun qr-match-p (query response)
  (= (pdu-request-id query) (pdu-request-id response)))

(defun oid-equal (oid1 oid2)
  (equal (object-id-subids oid1) (object-id-subids oid2)))

(defun check-query-response-match (session query response sender equalp)
  (declare (ignore sender))
  (labels ((response-error (type &rest args)
	     (apply 'error type
		    :session session
		    :query query
		    :response response
		    args)))
    ;; one day we should check whether the response comes from the
    ;; address that we had sent the query to.
    (let ((q-bindings (pdu-bindings query)))
      (if (qr-match-p query response)
	  (let ((r-bindings (pdu-bindings response)))
	    (do ((r-bindings r-bindings (rest r-bindings))
		 (q-bindings q-bindings (rest q-bindings)))
		((or (endp r-bindings) (endp q-bindings))
		 (cond ((not (endp q-bindings))
			(response-error 'snmp-response-too-short-error))
		       ((not (endp r-bindings))
			(response-error 'snmp-response-too-long-error))))
	      (when equalp
		(unless (oid-equal (first (first r-bindings)) 
				   (first (first q-bindings)))
		  (response-error 'snmp-response-attribute-mismatch-error)))))
	(response-error 'snmp-response-id-mismatch-error)))))

(defun check-response (session query response)
  (when (/= (pdu-error-status response) error-status-no-error)
    (error (make-snmp-response-error session query response))))
	   
(defmethod snmp-request ((s string) request bindings &optional (equalp nil))
  (multiple-value-bind (host community)
      (string->host/community s)
    (with-snmp-session (session :remote-host host :snmp-community community)
      (snmp-request session request bindings equalp))))

(defmethod snmp-request ((s snmpxc-session) req bindings &optional (equalp nil))
  (query->response s (make-snmp-query s req bindings 0 0) equalp))

(defmethod make-snmp-query ((s snmpxc-session) request-code bindings i0 i1)
  (list (snmp-session-snmp-version s)
	(snmpxc-session-snmp-community s)
	(make-typed-asn-tuple
	 request-code
	 (list
	  (snmp-session-next-request-id s) i0 i1
	  bindings))))

#+Genera
(defun query->response (session query equalp)
  (let ((response-pdu
	 (net:invoke-service-access-path
	  (udp-session-remote-service-access-path session)
	  (encode-pdu query))))
    (and response-pdu
	 (let ((response (decode-pdu response-pdu)))
	   (check-query-response-match session query response nil equalp)
	   (check-response session query response)
	   (pdu-bindings response)))))

#-Genera
(progn

(defmethod query->response ((s snmpxc-session) query equalp)
  (snmp-send-query s (encode-pdu query))
  (loop
   (handler-case
       (multiple-value-bind (response-pdu start end responder-address)
	   (snmp-receive-response s)
	 (return
	   (and response-pdu
		(let ((response (decode-pdu response-pdu :start start :end end)))
		  (check-query-response-match 
		   s
		   query response
		   responder-address equalp)
		  (check-response s query response)
		  (pdu-bindings response)))))
     (snmp-response-id-mismatch-error (c)
       (declare (ignore c))))))

(defun snmp-send-query (session query)
  (send-packet session query))

(defun snmp-receive-response (session)
  (and (wait-for-response session)
       (receive-packet session)))
);; end of #-Genera

#+excl
(defun wait-for-response (session)
  (mp:wait-for-input-available
   #+(and allegro (version>= 5 0)) (socket:socket-os-fd (udp-session-socket session))
   #-(and allegro (version>= 5 0)) (udp-session-socket session)
   :timeout (snmp-session-timeout session)
   :whostate "waiting for SNMP response to arrive"))

#+CMU
(defun wait-for-response (session)
  (system:wait-until-fd-usable
   (udp-session-socket session)
   :input
   (snmp-session-timeout session)))

#+:sbcl
(defun wait-for-response (session)
  (sb-sys:wait-until-fd-usable
   (sb-bsd-sockets:socket-file-descriptor
    (udp-session-socket session))
   :input
   (snmp-session-timeout session)))
