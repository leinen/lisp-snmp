;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File Name:	  snmp-server.lisp
;;; Description:  SNMP Server for Genera
;;; Author:	  Simon Leinen (simon@liasun1)
;;; Date Created: 30-May-92
;;; RCS $Header: /home/leinen/CVS/lisp-snmp/snmp/server.lisp,v 1.1 1992/05/30 18:57:08 simon Exp $  
;;; RCS $Log: server.lisp,v $
;;; RCS Revision 1.1  1992/05/30 18:57:08  simon
;;; RCS Initial revision
;;; RCS	  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package "SNMP")

(defconstant default-snmp-port 161)

(net:define-protocol :snmp (:network-management :datagram)
  (:invoke-with-stream-and-close (stream)
    (break "SNMP invoked: ~S ~S ~S" stream)))

(tcp:add-udp-port-for-protocol :snmp default-snmp-port)

(net:define-server :snmp (:medium :datagram
			  :request-array (request start end))
   (break "SNMP server called: ~S [~D:~D]" request start end))
