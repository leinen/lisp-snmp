;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File Name:	  defsys.lisp
;;; Description:  System description for SYSMAN
;;; Author:	  Simon Leinen (simon@liasun1)
;;; Date Created: 28-May-92
;;; RCS $Header: /home/leinen/CVS/lisp-snmp/defsys.lisp,v 1.26 2002/08/11 21:50:16 leinen Exp $  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#+excl
(excl:defsystem :sysman
    (:pretty-name "System Management Substrate"
     :default-pathname "sysman:src;"
     :default-file-type "lisp")
  ("sysman:src;asn1;package")
  ("sysman:src;snmp;package"
   (:uses-definitions-from "sysman:src;asn1;package"))
  ("sysman:src;dependent"
   (:in-order-to :compile (:load "sysman:src;snmp;package"))
   (:in-order-to :load (:load "sysman:src;asn1;package")))
  ("sysman:src;asn1;defs"
   (:in-order-to :compile (:load "sysman:src;asn1;package" "sysman:src;dependent"))
   (:in-order-to :load (:load "sysman:src;asn1;package")))
  ("sysman:src;ber;defs" 
   (:uses-definitions-from "sysman:src;asn1;package" "sysman:src;asn1;defs"))
  ("sysman:src;ber;decode"
   (:uses-definitions-from ("sysman:src;asn1;package" "sysman:src;asn1;defs" "sysman:src;ber;defs")))
  ("sysman:src;ber;encode"
   (:uses-definitions-from ("sysman:src;asn1;package" "sysman:src;asn1;defs" "sysman:src;ber;defs")))
  ("sysman:src;mib;defs"
   (:in-order-to :compile (:load "sysman:src;snmp;package" "sysman:src;dependent")))
  ("sysman:src;mib;read"
   (:in-order-to :compile (:load "sysman:src;snmp;package" "sysman:src;dependent" "sysman:src;mib;defs")))
  ("sysman:src;snmp;low"
   (:in-order-to :compile (:load "sysman:src;snmp;package" "sysman:src;dependent"))
   (:in-order-to :load (:load "sysman:src;snmp;package")))
  ("sysman:src;snmp;pdu"
   (:in-order-to :compile (:load "sysman:src;snmp;package" "sysman:src;dependent" "sysman:src;snmp;low")))
  ("sysman:src;low;ip"
   (:in-order-to :compile (:load "sysman:src;snmp;package" "sysman:src;dependent")))
  ("sysman:src;low;udp"
   (:in-order-to :compile (:load "sysman:src;snmp;package" "sysman:src;dependent" "sysman:src;low;ip")))
  ("sysman:src;snmp;session"
   (:in-order-to :compile (:load "sysman:src;snmp;package" "sysman:src;dependent"
				 "sysman:src;low;ip" "sysman:src;low;udp" "sysman:src;snmp;pdu")))
  ("sysman:src;snmp;errors"
   (:in-order-to :compile (:load "sysman:src;snmp;package" "sysman:src;dependent"
				 "sysman:src;low;ip" "sysman:src;low;udp" "sysman:src;snmp;pdu")))
  ("sysman:src;snmp;api"
   (:uses-definitions-from "sysman:src;snmp;package" "sysman:src;dependent"
			   "sysman:src;low;ip" "sysman:src;low;udp" "sysman:src;snmp;pdu" "sysman:src;snmp;session"))
  ("sysman:src;mib;oid-reader"
   (:in-order-to :compile (:load "sysman:src;snmp;package" "sysman:src;dependent" "sysman:src;mib;defs"))
   (:in-order-to :load (:load "sysman:src;snmp;package" "sysman:src;asn1;package" "sysman:src;asn1;defs" "sysman:src;mib;defs")))   
  ("mib-2"
   (:in-order-to :compile (:load "sysman:src;snmp;package" "sysman:src;dependent" "sysman:src;mib;defs" "sysman:src;mib;read")))
  ("sysman:src;snmp;netstat"
   (:in-order-to :compile (:load "sysman:src;snmp;package" "sysman:src;dependent" "sysman:src;snmp;api" "sysman:src;mib;oid-reader" "mib-2")))
  ("test"
   (:in-order-to :compile (:load "sysman:src;snmp;package" "sysman:src;dependent" "sysman:src;snmp;api"))))

#+excl
(excl:defsystem :snmp-agent
    (:pretty-name "SNMP Agent")
  (:serial
   :snmp
   (:serial
    "sysman:src;agent;generic;mib-instr"
    "sysman:src;agent;generic;errors"
    (:parallel
     "sysman:src;agent;generic;main"
     "sysman:src;agent;generic;mib-2"))))

#+Genera
(sct:defsystem sysman
    (:pretty-name "System Management Substrate"
		  :default-pathname "sysman:src;"
		  :bug-reports ("simon@switch.ch")
		  :initial-status :experimental)
  (:module asn1-package ("sysman:src;asn1;package.lisp"))
  (:module snmp-package ("sysman:src;snmp;package.lisp")
	   (:uses-definitions-from asn1-package))
  (:module dependent ("dependent.lisp")
	   (:uses-definitions-from snmp-package))
  (:module ip ("sysman:src;low;ip.lisp")
	   (:uses-definitions-from snmp-package dependent))
  (:module snmp-low ("sysman:src;snmp;low.lisp")
	   (:uses-definitions-from snmp-package dependent))
  (:module udp ("sysman:src;low;udp.lisp")
	   (:uses-definitions-from snmp-package dependent ip))
  (:module asn1-defs ("sysman:src;asn1;defs.lisp")
	   (:uses-definitions-from asn1-package snmp-package dependent))
  (:module ber-defs ("sysman:src;ber;defs.lisp")
	   (:uses-definitions-from asn1-package dependent))
  (:module ber-decode ("sysman:src;ber;decode.lisp")
	   (:uses-definitions-from asn1-package asn1-defs ber-defs))
  (:module ber-encode ("sysman:src;ber;encode.lisp")
	   (:uses-definitions-from asn1-package asn1-defs ber-defs))
  (:module mib ("sysman:src;mib;defs.lisp")
	   (:uses-definitions-from asn1-package snmp-package dependent))
  (:module read-mib ("sysman:src;mib;read.lisp")
	   (:uses-definitions-from asn1-package snmp-package dependent mib))
  (:module mib-instr ("sysman:src;agent;generic;mib-instr.lisp")
	   (:uses-definitions-from mib))
  (:module mib-2 ("mib-2.lisp")
	   (:uses-definitions-from snmp-package dependent read-mib))
  (:module oid-reader ("sysman:src;mib;oid-reader.lisp"))
  (:module snmp-pdu ("sysman:src;snmp;pdu.lisp")
	   (:uses-definitions-from asn1-package snmp-package dependent))
  (:module snmp-session ("sysman:src;snmp;session.lisp")
	   (:uses-definitions-from asn1-package
				   snmp-package
				   dependent
				   udp))
  (:module snmp-errors ("sysman:src;snmp;errors.lisp")
	   (:uses-definitions-from asn1-package
				   snmp-package
				   dependent
				   udp))
  (:module snmp-api ("sysman:src;snmp;api.lisp")
	   (:uses-definitions-from asn1-package
				   snmp-package
				   dependent
				   udp
				   snmp-session))
  (:module agent ("sysman:agent;generic;main")
	   (:uses-definitions-from asn1-package
				   snmp-package
				   dependent
				   snmp-pdu
				   snmp-api
				   udp))
  (:module lispm-agent-mib-2 ("sysman:agent;lispm;mib-2.lisp")
	   (:uses-definitions-from
	    oid-reader mib-2 agent mib-instr))
  (:module snmp-netstat ("sysman:src;snmp;netstat.lisp")
	   (:uses-definitions-from snmp-package
				   dependent
				   snmp-pdu
				   snmp-api
				   mib-2
				   oid-reader)))

#+(and CMU FOO)
(make:defsystem :sysman
    (:pretty-name "System Management Substrate")
  (:serial "sysman:src;asn1;package"
	   "sysman:src;snmp;package"
	   "sysman:src;dependent"
	   (:serial "sysman:src;asn1;defs"
		    "sysman:src;ber;defs"
		    (:parallel
		     "sysman:src;ber;decode"
		     "sysman:src;ber;encode"))
	   (:serial
	    "sysman:src;mib;defs"
	    "sysman:src;mib;read"
	    "mib-2"
	    "sysman:src;snmp;pdu"
	    (:serial "sysman:src;low;ip" "sysman:src;low;udp" "sysman:src;snmp;session" "sysman:src;snmp;errors" "sysman:src;snmp;api")
	    "sysman:src;snmp;low")
	   "sysman:src;mib;oid-reader"
	   "mib-2"
	   "sysman:src;snmp;netstat"
	   "test"))

#+CMU
(make:defsystem :sysman
    :source-pathname "sysman:src;"
    :binary-pathname "sysman:src;"
    :source-extension "lisp"
    :binary-extension #+SGI "sgif" #+SPARC "sparcf"
    :components 
    ((:file "sysman:src;asn1;package")
     (:file "sysman:src;dependent"
	    :depends-on ("sysman:src;asn1;package" "sysman:src;snmp;package"))
     (:file "sysman:src;asn1;defs"
	    :depends-on ("sysman:src;asn1;package" "sysman:src;dependent"))
     (:file "sysman:src;ber;defs"
	    :depends-on ("sysman:src;asn1;package" "sysman:src;asn1;defs"))
     (:file "sysman:src;ber;decode"
	    :depends-on ("sysman:src;asn1;package" "sysman:src;asn1;defs" "sysman:src;ber;defs"))
     (:file "sysman:src;ber;encode"
	    :depends-on ("sysman:src;asn1;package" "sysman:src;asn1;defs" "sysman:src;ber;defs"))
     (:file "sysman:src;snmp;package" :depends-on ("sysman:src;asn1;package"))
     (:file "sysman:src;mib;defs"
	    :depends-on ("sysman:src;snmp;package" "sysman:src;dependent"))
     (:file "sysman:src;mib;read"
	    :depends-on ("sysman:src;snmp;package" "sysman:src;dependent" "sysman:src;mib;defs"))
     (:file "mib-2"
	    :depends-on ("sysman:src;snmp;package" "sysman:src;dependent" "sysman:src;mib;defs" "sysman:src;mib;read"))
     (:file "sysman:src;snmp;pdu"
	    :depends-on ("sysman:src;snmp;package" "sysman:src;dependent" "sysman:src;snmp;low"))
     (:file "sysman:src;low;ip"
	    :depends-on ("sysman:src;snmp;package" "sysman:src;dependent"))
     (:file "sysman:src;low;udp"
	    :depends-on ("sysman:src;snmp;package" "sysman:src;dependent" "sysman:src;low;ip"))
     (:file "sysman:src;snmp;session"
	    :depends-on ("sysman:src;snmp;package" "sysman:src;dependent" "sysman:src;low;ip"
			 "sysman:src;low;udp" "sysman:src;snmp;pdu"))
     (:file "sysman:src;snmp;errors"
	    :depends-on ("sysman:src;snmp;package" "sysman:src;dependent" "sysman:src;low;ip"
			 "sysman:src;low;udp" "sysman:src;snmp;pdu"))
     (:file "sysman:src;snmp;api"
	    :depends-on ("sysman:src;snmp;package" "sysman:src;dependent" "sysman:src;low;ip"
			 "sysman:src;low;udp" "sysman:src;snmp;pdu" "sysman:src;snmp;session"))
     (:file "sysman:src;snmp;low"
	    :depends-on ("sysman:src;snmp;package" "sysman:src;dependent"))
     (:file "sysman:src;mib;oid-reader"
	    :depends-on ("sysman:src;snmp;package" "sysman:src;dependent" "sysman:src;mib;defs"))
     (:file "sysman:src;snmp;netstat"
	    :depends-on ("sysman:src;snmp;package" "sysman:src;dependent" "sysman:src;snmp;api"))
     (:file "test"
	    :depends-on ("sysman:src;snmp;package" "sysman:src;dependent" "sysman:src;snmp;api"))))

#-(or excl Genera CMU)
(dolist (file '("sysman:src;asn1;package"
		"sysman:src;snmp;package" "sysman:src;dependent"
		"sysman:src;asn1;defs"
		"sysman:src;ber;defs" "sysman:src;ber;decode" "sysman:src;ber;encode"
		"sysman:src;sysman:src;mib;defs" "sysman:src;mib;read"
		"sysman:src;snmp;low" "sysman:src;snmp;pdu"
		"sysman:src;low;ip" "sysman:src;low;udp" 
		"sysman:src;snmp;session" "sysman:src;snmp;errors" "sysman:src;snmp;api"
		"sysman:src;mib;oid-reader"
		"mib-2" "sysman:src;snmp;netstat" "test"))
  (compile-file file)
  (load file))
