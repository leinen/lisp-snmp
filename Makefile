default: dist

            pkg = sysman

        PACKAGE = lisp-snmp
             RM = rm -f
             CC = gcc
             LD = ld
  LDSHAREDFLAGS = -shared
   CSHAREDFLAGS = -fpic
    CDEBUGFLAGS = -O -g -W
         CFLAGS = $(CDEBUGFLAGS) $(CSHAREDFLAGS)
          ETAGS = etags

.SUFFIXES: .so

LISP_SOURCES = \
	defsys.lisp \
	asn1/package.lisp snmp/package.lisp \
	asn1/defs.lisp dependent.lisp \
	ber/defs.lisp ber/decode.lisp ber/encode.lisp \
	low/ip.lisp low/udp.lisp \
	mib/defs.lisp mib/read.lisp mib/oid-reader.lisp \
	agent/generic/mib-instr.lisp \
	agent/generic/main.lisp agent/generic/mib-2.lisp \
	snmp/low.lisp snmp/pdu.lisp \
	snmp/session.lisp snmp/errors.lisp snmp/api.lisp \
	snmp/v2p/party.lisp snmp/v2p/context.lisp snmp/v2p/errors.lisp \
	snmp/v2p/api.lisp \
	mib-2.lisp snmp/netstat.lisp \
	agent/lispm/lispm-agent.lisp agent/lispm/lispm-agent-mib-2.lisp \
	agent/sgi/sgi-agent.lisp \
	test.lisp
###	rfc1514.lisp

ALL_SRCS = Makefile $(LISP_SOURCES) objects.defs doc/sysman.texi

DISTRIBUTABLES = README \
	$(LISP_SOURCES) objects.defs \
	doc/sysman.texi doc/sysman.ps

dist: $(DISTRIBUTABLES)
	-$(RM) ,foo
	tar cf - $(DISTRIBUTABLES) | gzip > ,foo \
	&& mv ,foo $(PACKAGE).tar.gz \
	&& ls -l $(PACKAGE).tar.gz

targz: ../$(pkg).tar.gz

../$(pkg).tar.gz: $(ALL_SRCS) RCS/*
	( cd .. && tar cf - $(patsubst %,$(pkg)/%,$(ALL_SRCS)) $(pkg)/RCS ) | gzip > ../$(pkg).tar.gz

.o.so:
	$(LD) $(LDSHAREDFLAGS) -o $@ $<

tags: TAGS

TAGS: $(LISP_SOURCES)
	$(ETAGS) $(LISP_SOURCES)

doc:; cd doc && $(MAKE) $(MFLAGS);
doc/sysman.ps: doc/sysman.texi; cd doc && $(MAKE) $(MFLAGS) sysman.ps

clean:
	$(RM) *.bin *.ibin *.sparcf *.sgif *.fasl *.mfasl *.o *.so
	$(RM) *~
	$(RM) TAGS
